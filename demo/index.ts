import { ComponentBase, customElement, property, html, css } from "../package/index.js";

@customElement('click-counter')
//extending mixed in class works :thinking:
class ClickCounter extends ComponentBase {

	@property({
		type: Number
	})
	counter: number = 0;

	increase() {
		this.counter++;
	}
	
	decrease() {
		if(this.counter !== 0) this.counter--;
	}

	static styles = [
		css`
			:host {
				display: inline-block;
				background: #dddada;
				border-radius: 3px;
				padding: 1rem;
			}
		`
	]

	updated(prop, val) {
		console.log(prop, val)
	}

	render() {
		return html`
			<h2>Counter Component</h2>
			<div>
				<span>Count: ${this.counter}</span>
				<button @click=${this.increase}>+</button>
				<button @click=${this.decrease}>-</button>
			</div>
		`;
	}
}