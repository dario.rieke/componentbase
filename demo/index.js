(() => {
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __decorateClass = (decorators, target, key, kind) => {
    var result = kind > 1 ? void 0 : kind ? __getOwnPropDesc(target, key) : target;
    for (var i = decorators.length - 1, decorator; i >= 0; i--)
      if (decorator = decorators[i])
        result = (kind ? decorator(target, key, result) : decorator(result)) || result;
    if (kind && result)
      __defProp(target, key, result);
    return result;
  };

  // package/decorators/custom-element.js
  var customElement = (tagname) => (cls) => {
    window.customElements.define(tagname, cls);
  };

  // package/template/TemplatePart.js
  var Part = class {
    constructor(id, value) {
      this.value = value;
      this.id = id;
    }
    update(value) {
      this.value = value;
    }
    getValue() {
      return this.value;
    }
    setValue(value) {
      this.value = value;
    }
    clone() {
      return new this.constructor(this.id, this.value);
    }
    hasChanged(value) {
      return value !== this.value;
    }
  };
  var NodePart = class extends Part {
    update(value) {
      const oldVal = this.value;
      super.update(value);
      if (value instanceof TemplateResult) {
        if (oldVal instanceof TemplateResult && value.compiledTemplate !== oldVal.compiledTemplate || isTemplateResultIterable(value)) {
          const content = initialRender(value);
          removeBetweenNodes(this.start, this.end);
          this.end.parentNode.insertBefore(content, this.end);
          this.childParts = value.parts;
        }
        if (this.referenceNode !== null) {
          const content = initialRender(value);
          this.referenceNode.replaceWith(content);
          this.referenceNode = null;
          this.childParts = value.parts;
        }
        updateParts(this.childParts, value.values);
      } else if (isTemplateResultIterable(value)) {
        removeBetweenNodes(this.start, this.end);
        let templateResult;
        for (templateResult of value) {
          const fragment = initialRender(templateResult);
          this.end.parentNode.insertBefore(fragment, this.end);
        }
      } else {
        if (oldVal instanceof TemplateResult || isTemplateResultIterable(value)) {
          removeBetweenNodes(this.start, this.end);
          const textNode = document.createTextNode(value);
          this.end.parentNode.insertBefore(textNode, this.end);
          this.referenceNode = textNode;
        } else {
          this.referenceNode.textContent = value;
        }
      }
    }
    clone() {
      const _new = new this.constructor(this.id, this.value);
      _new.start = this.start;
      _new.referenceNode = this.referenceNode;
      _new.end = this.end;
      _new.childParts = this.childParts;
      return _new;
    }
  };
  var AttributeNamePart = class extends Part {
    update(value) {
      const oldAttrName = this.value;
      super.update(value);
      if (this.referenceNode.hasAttribute(oldAttrName)) {
        this.attributeValue = this.referenceNode.getAttribute(oldAttrName);
      }
      this.referenceNode.removeAttribute(oldAttrName);
      if (value)
        this.referenceNode.setAttribute(value, this.attributeValue);
    }
    clone() {
      const _new = new this.constructor(this.id, this.value);
      _new.attributeValue = this.attributeValue;
      return _new;
    }
  };
  var AttributeValuePart = class extends Part {
    update(value) {
      const oldValue = String(this.value);
      super.update(value);
      const attributeValue = this.referenceNode.getAttribute(this.attributeName);
      const newVal = attributeValue.substring(0, this.start) + value + attributeValue.substring(this.end);
      this.referenceNode.setAttribute(this.attributeName, newVal);
      this.start = attributeValue.substring(0, this.start).length;
      this.end = this.start + value.length;
    }
    clone() {
      const _new = new this.constructor(this.id, this.value);
      _new.attributeName = this.attributeName;
      _new.start = this.start;
      _new.end = this.end;
      return _new;
    }
  };
  var EventListenerPart = class extends Part {
    update(value) {
      const oldVal = this.value;
    }
    clone() {
      const _new = new this.constructor(this.id, this.value);
      _new.eventName = this.eventName;
      return _new;
    }
  };
  function updateParts(parts, values) {
    let index = 0;
    parts.forEach((part) => {
      const newValue = resolveValue(values[index]);
      if (part.hasChanged(newValue)) {
        part.update(newValue);
      }
      index++;
    });
  }
  function isTemplateResultIterable(value) {
    return typeof value[Symbol.iterator] === "function" && value[0] instanceof TemplateResult;
  }

  // package/template/CompiledTemplate.js
  var MATCH_PLACEHOLDER = /(part_[0-9]+_[a-z0-9]+_)/g;
  var CompiledTemplate = class {
    constructor(strings, values) {
      this.parts = /* @__PURE__ */ new Map();
      this.partIds = [];
      var result = strings[0];
      for (var l = 0; l < values.length; ++l) {
        let id = `part_${l}_${generateUniqueString()}_`;
        this.partIds.push(id);
        result += id + strings[l + 1];
      }
      const tmpl = document.createElement("template");
      tmpl.innerHTML = result;
      const nodeIterator = document.createNodeIterator(tmpl.content, NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT);
      var currentNode;
      var partIndex = -1;
      while (nodeIterator.nextNode()) {
        switch (nodeIterator.referenceNode.nodeType) {
          case Node.TEXT_NODE:
            currentNode = nodeIterator.referenceNode;
            if (currentNode.nodeValue.includes("part_")) {
              const parts = currentNode.nodeValue.split(MATCH_PLACEHOLDER);
              parts.forEach((text) => {
                let newNode;
                if (text.includes("part_")) {
                  partIndex++;
                  newNode = document.createComment(text);
                  const part = new NodePart(this.partIds[partIndex], resolveValue(values[partIndex]));
                  const startMarker = document.createTextNode(" ");
                  const endMarker = document.createTextNode(" ");
                  this.parts.set(this.partIds[partIndex], part);
                  currentNode.parentNode.insertBefore(startMarker, currentNode);
                  currentNode.parentNode.insertBefore(newNode, currentNode);
                  currentNode.parentNode.insertBefore(endMarker, currentNode);
                } else {
                  newNode = document.createTextNode(text);
                  currentNode.parentNode.insertBefore(newNode, currentNode);
                }
              });
              currentNode.remove();
            }
            break;
          case Node.ELEMENT_NODE:
            currentNode = nodeIterator.referenceNode;
            const attributes = Array.from(currentNode.attributes);
            const attrCount = attributes.length;
            for (let count = 0; count < attrCount; count++) {
              const attribute = attributes[count];
              if (attribute.value.includes("part_")) {
                if (!currentNode.hasAttribute("has_attribute_parts"))
                  currentNode.setAttribute("has_attribute_parts", "");
                if (attribute.name.startsWith("@")) {
                  partIndex++;
                  const eventName = attribute.name.substring(1);
                  if (typeof values[partIndex] !== "function")
                    throw TypeError("Event listener registered with @EVENT is not a function.");
                  const id = this.partIds[partIndex];
                  const listener = resolveValue(values[partIndex]);
                  const part = new EventListenerPart(id, listener);
                  part.eventName = eventName;
                  this.parts.set(this.partIds[partIndex], part);
                } else {
                  let valueMatches = attribute.value.split(MATCH_PLACEHOLDER);
                  valueMatches.forEach((text) => {
                    if (text.includes("part_")) {
                      partIndex++;
                      const id = this.partIds[partIndex];
                      const part = new AttributeValuePart(id, resolveValue(values[partIndex]));
                      part.attributeName = resolveAttributeName(attribute.name);
                      this.parts.set(this.partIds[partIndex], part);
                    }
                  });
                }
              }
              if (attribute.name.includes("part_")) {
                partIndex++;
                if (!currentNode.hasAttribute("has_attribute_parts"))
                  currentNode.setAttribute("has_attribute_parts", "");
                const initialVal = currentNode.hasAttribute(attribute.name) ? currentNode.getAttribute(attribute.name) : "";
                const id = this.partIds[partIndex];
                const attributeName = resolveAttributeName(values[partIndex]);
                const part = new AttributeNamePart(this.partIds[partIndex], attributeName);
                part.referenceNode = currentNode;
                part.attributeValue = resolveValue(initialVal);
                this.parts.set(this.partIds[partIndex], part);
              }
            }
            break;
          default:
            break;
        }
      }
      this.templateElement = tmpl;
    }
    getTemplateElement() {
      return document.importNode(this.templateElement, true);
    }
    getParts(values) {
      const cloned = /* @__PURE__ */ new Map();
      let i = 0;
      this.parts.forEach((part, id) => {
        const clonedPart = part.clone();
        clonedPart.setValue(values[i]);
        cloned.set(id, clonedPart);
        i++;
      });
      return cloned;
    }
    getPartIds() {
      return this.partIds;
    }
  };

  // package/template/TemplateResult.js
  var cachedParts = /* @__PURE__ */ new Map();
  var TemplateResult = class {
    constructor(compiledTemplate, values) {
      this._parts = /* @__PURE__ */ new Map();
      this.compiledTemplate = compiledTemplate;
      this.values = values;
    }
    set parts(value) {
      this._parts = value;
    }
    get parts() {
      return this._parts;
    }
  };
  function needsInitialRender(template, elementToRenderTo) {
    return !cachedParts.has(elementToRenderTo);
  }
  function initialRender(template, elementToRenderTo, eventContext) {
    const parts = template.compiledTemplate.getParts(template.values);
    const templateEl = template.compiledTemplate.getTemplateElement();
    const nodeIterator = document.createNodeIterator(templateEl.content, NodeFilter.SHOW_COMMENT | NodeFilter.SHOW_ELEMENT);
    while (nodeIterator.nextNode()) {
      switch (nodeIterator.referenceNode.nodeType) {
        case Node.COMMENT_NODE:
          const comment = nodeIterator.referenceNode;
          if (comment.nodeValue.includes("part_")) {
            const id = comment.nodeValue.trim();
            const part = parts.get(id);
            const value = part.getValue();
            part.start = comment.previousSibling;
            part.end = comment.nextSibling;
            const resolved = resolveValue(value);
            if (resolved instanceof TemplateResult) {
              const content = initialRender(resolved, null, eventContext);
              part.end.parentNode.insertBefore(content, part.end);
              comment.remove();
              part.referenceNode = null;
              part.childParts = resolved.parts;
            } else if (isTemplateResultIterable(resolved)) {
              let templateResult;
              const contents = [];
              for (templateResult of resolved) {
                contents.push(initialRender(templateResult, null, eventContext));
              }
              comment.replaceWith(...contents);
              part.referenceNode = null;
              part.childTemplates = resolved;
            } else {
              const newNode = document.createTextNode(resolved);
              part.referenceNode = newNode;
              comment.replaceWith(newNode);
            }
          }
          break;
        case Node.ELEMENT_NODE:
          const element = nodeIterator.referenceNode;
          if (element.hasAttribute("has_attribute_parts")) {
            element.removeAttribute("has_attribute_parts");
            const attributes = Array.from(element.attributes);
            const attrLength = attributes.length;
            for (let i = 0; i < attrLength; i++) {
              const attribute = attributes[i];
              if (attribute.name.includes("part_")) {
                const part = parts.get(attribute.name);
                part.referenceNode = element;
                element.removeAttribute(attribute.name);
                const attrName = resolveValue(part.getValue());
                if (attrName.length)
                  element.setAttribute(attrName, part.attributeValue);
              }
              if (attribute.value.includes("part_")) {
                if (attribute.name.startsWith("@")) {
                  const part = parts.get(attribute.value);
                  let value = part.getValue();
                  part.referenceNode = element;
                  value = value.bind(eventContext);
                  part.setValue(value);
                  element.removeAttribute(attribute.name);
                  element.addEventListener(part.eventName, value);
                  if (!element.hasOwnProperty("_eventListeners"))
                    element._eventListeners = /* @__PURE__ */ new Map();
                  element._eventListeners.set(part.eventName, value);
                } else {
                  let valueMatches = attribute.value.split(MATCH_PLACEHOLDER);
                  let attrValue = "";
                  valueMatches.forEach((text) => {
                    if (text.includes("part_")) {
                      const part = parts.get(text);
                      const value = resolveValue(part.getValue());
                      part.referenceNode = element;
                      part.start = attrValue.length;
                      attrValue += value;
                      part.end = part.start + value.length;
                    } else
                      attrValue += text;
                  });
                  element.setAttribute(attribute.name, attrValue);
                }
              }
            }
          }
          break;
        default:
          break;
      }
    }
    template.parts = parts;
    if (elementToRenderTo) {
      cachedParts.set(elementToRenderTo, parts);
    }
    return templateEl.content;
  }
  function render(template, elementToRenderTo, eventContext = null) {
    if (needsInitialRender(template, elementToRenderTo)) {
      const renderedContent = initialRender(template, elementToRenderTo, eventContext);
      elementToRenderTo.appendChild(renderedContent);
    } else {
      const parts = cachedParts.get(elementToRenderTo);
      const values = template.values;
      updateParts(parts, values);
    }
  }

  // package/template/util.js
  function generateUniqueString() {
    const dateString = Date.now().toString(36);
    const randomness = Math.random().toString(36).substr(2);
    const result = dateString + randomness;
    return result.length > 19 ? result.substr(1) : result;
  }
  function resolveValue(value) {
    switch (typeof value) {
      case "string":
        return escape(String(value));
        break;
      case "number":
        return escape(String(value));
      case "boolean":
        return escape(String(value));
      case "object":
        if (typeof value[Symbol.iterator] === "function") {
          if (value[0] instanceof TemplateResult)
            return value;
          return escape(value.join(" "));
        } else if (value instanceof Object) {
          if (value instanceof TemplateResult)
            return value;
          return JSON.stringify(value);
        }
        return value;
        break;
      case "function":
        return value;
      case "undefined":
        return "";
        break;
      default:
        break;
    }
  }
  function resolveAttributeName(value) {
    return value.toLowerCase();
  }
  function escape(string) {
    return string;
  }
  function removeBetweenNodes(start, end) {
    let currentNode = start.nextSibling;
    while (currentNode) {
      if (currentNode === end)
        break;
      if (currentNode.hasOwnProperty("_eventListeners")) {
        currentNode._eventListeners.forEach((listener, eventName) => {
          currentNode.removeEventListener(eventName, listener);
        });
      }
      const nextSibling = currentNode.nextSibling;
      currentNode.parentNode.removeChild(currentNode);
      currentNode = nextSibling;
    }
  }

  // package/PropertyOptions.js
  var defaultPropertyToAttributeNameConverter = (propName) => propName.toString().toLowerCase();
  var DefaultPropertyOptions = {
    get: function(prop) {
      return this._properties.get(prop);
    },
    set: function(prop, value) {
      this._properties.set(prop, value);
      this.update(prop, value);
    },
    toAttribute: function(value, type) {
      switch (type) {
        case String:
        case Boolean:
        case Number:
          return value;
        case Object:
        case Array:
          return resolveValue(JSON.stringify(value));
        default:
          return value;
      }
    },
    toProperty: function(value, type) {
      switch (type) {
        case String:
          return value;
        case Boolean:
          if (value !== null || value === "")
            return true;
          else
            return false;
        case Object:
        case Array:
          return JSON.parse(value);
        case Number:
          return Number(value);
        default:
          return value;
      }
    },
    type: String,
    emitEvent: false,
    attribute: true,
    attributeName: defaultPropertyToAttributeNameConverter,
    hasChanged: function(oldV, newV, options) {
      if (this._isUpdating)
        return false;
      switch (options.type) {
        case Boolean:
          return newV === null || oldV === null;
        case Array:
          return oldV === null && newV !== null || oldV.length !== newV.length || oldV !== newV;
        default:
          return oldV !== newV;
      }
    }
  };

  // package/decorators/property.js
  var property = (options = DefaultPropertyOptions) => {
    if (options !== DefaultPropertyOptions)
      options = Object.assign(Object.assign({}, DefaultPropertyOptions), options);
    return (target, prop, descriptor) => {
      const ctor = target.constructor;
      if (!Object.getOwnPropertyDescriptor(ctor, "properties")) {
        if (ctor.properties)
          ctor.properties = new Map(ctor.properties);
        else
          ctor.properties = /* @__PURE__ */ new Map();
      }
      ctor.properties.set(prop, options);
    };
  };

  // package/UpdatingElement.js
  var getChangeEvent = (prop, value) => new CustomEvent(prop.toString(), { detail: value });
  var UpdatingElement = class extends HTMLElement {
    constructor() {
      super();
      this._properties = /* @__PURE__ */ new Map();
      this._isUpdating = null;
    }
    static get observedAttributes() {
      this.finalize();
      const attributes = [];
      this.properties.forEach((options, key) => {
        attributes.push(key.toString().toLowerCase());
      });
      return attributes;
    }
    static finalize() {
      var _a;
      if (this.hasOwnProperty("_finalized"))
        return;
      this._finalized = true;
      const finalPropMap = new Map([...(_a = Object.getPrototypeOf(this)) === null || _a === void 0 ? void 0 : _a.properties, ...this.properties]);
      this.properties = finalPropMap;
      this._attributePropertyMap = /* @__PURE__ */ new Map();
      for (let [prop, options] of finalPropMap) {
        this._attributePropertyMap.set(options.attributeName(prop), prop);
        this.createProperty(prop, options);
      }
    }
    static createProperty(prop, options) {
      this.finalize();
      const getter = function() {
        return options.get.call(this, prop);
      };
      const setter = function(val) {
        if (options.emitEvent) {
          const event = getChangeEvent(prop, val);
          this.dispatchEvent(event);
        }
        options.set.call(this, prop, val);
      };
      Object.defineProperty(this.prototype, prop, {
        get: getter,
        set: setter
      });
    }
    attributeChangedCallback(attr, oldValue, newValue) {
      const prop = this.constructor._attributePropertyMap.get(attr);
      const options = this.constructor.properties.get(prop);
      if (options.hasChanged.call(this, oldValue, newValue, options))
        this[prop] = options.toProperty(newValue, options.type);
    }
    static getAttributeName(prop) {
      return String(prop).toLowerCase();
    }
    update(prop, value) {
      this._isUpdating = prop;
      const options = this.constructor.properties.get(prop);
      if (options.attribute) {
        switch (options.type) {
          case Boolean:
            this.toggleAttribute(options.attributeName(prop), Boolean(options.toAttribute(value, options.type)));
            break;
          default:
            this.setAttribute(options.attributeName(prop), options.toAttribute(value, options.type));
            break;
        }
      }
      this._isUpdating = null;
    }
  };
  UpdatingElement.properties = /* @__PURE__ */ new Map();
  UpdatingElement._attributePropertyMap = /* @__PURE__ */ new Map();

  // package/ComponentBase.js
  var __awaiter = function(thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P ? value : new P(function(resolve) {
        resolve(value);
      });
    }
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }
      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  };
  var ComponentBase = class extends UpdatingElement {
    constructor() {
      super();
      this._shouldRender = false;
      this.renderRoot = this.createRenderRoot();
    }
    createRenderRoot() {
      return this.attachShadow({
        mode: "open"
      });
    }
    connectedCallback() {
      this._shouldRender = true;
      this.firstRender = this._render();
      this.applyStylesheets();
    }
    update(prop, value) {
      super.update(prop, value);
      if (this._shouldRender) {
        this.reRender = this._render();
        this.reRender.then(() => {
          if ("updated" in this)
            this.updated(prop, value);
        });
      }
    }
    _render() {
      return __awaiter(this, void 0, void 0, function* () {
        if ("render" in this) {
          const template = this.render();
          render(template, this.renderRoot, this);
        }
        return Promise.resolve();
      });
    }
    applyStylesheets() {
      if (!("styles" in this.constructor))
        return;
      if (this.renderRoot.nodeName === "#document-fragment") {
        this.renderRoot.adoptedStyleSheets = this.constructor.styles;
      } else {
        console.warn(`
				${this.constructor} has static styles defined, 
				but the renderRoot is not a ShadowDom. 
				No styles will be inserted. You have to load them yourself.
			`);
        return;
      }
    }
  };

  // package/template/html.js
  var templates = /* @__PURE__ */ new WeakMap();
  function html(strings, ...values) {
    let template;
    if (!templates.has(strings)) {
      template = new CompiledTemplate(strings, values);
      templates.set(strings, template);
    } else {
      template = templates.get(strings);
    }
    return new TemplateResult(template, values);
  }

  // package/template/css.js
  function css(strings, ...values) {
    let result = strings[0];
    for (let l = 0; l < values.length; ++l) {
      result += values[l] + strings[l + 1];
    }
    const css2 = new CSSStyleSheet();
    css2.replaceSync(result);
    return css2;
  }

  // demo/index.ts
  var ClickCounter = class extends ComponentBase {
    constructor() {
      super(...arguments);
      this.counter = 0;
    }
    increase() {
      this.counter++;
    }
    decrease() {
      if (this.counter !== 0)
        this.counter--;
    }
    updated(prop, val) {
      console.log(prop, val);
    }
    render() {
      return html`
			<h2>Counter Component</h2>
			<div>
				<span>Count: ${this.counter}</span>
				<button @click=${this.increase}>+</button>
				<button @click=${this.decrease}>-</button>
			</div>
		`;
    }
  };
  ClickCounter.styles = [
    css`
			:host {
				display: inline-block;
				background: #dddada;
				border-radius: 3px;
				padding: 1rem;
			}
		`
  ];
  __decorateClass([
    property({
      type: Number
    })
  ], ClickCounter.prototype, "counter", 2);
  ClickCounter = __decorateClass([
    customElement("click-counter")
  ], ClickCounter);
})();
