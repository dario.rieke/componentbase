import { CompiledTemplate } from "./CompiledTemplate.js";
import { TemplateResult } from "./TemplateResult.js";

/**
 * weak cache for compiled templates
 */
const templates = new WeakMap<TemplateStringsArray, CompiledTemplate>();

/**
 * return a template which can be rendered to an element
 * @param strings 
 * @param values 
 * @returns 
 */
export function html(strings: TemplateStringsArray, ...values: any[]) {
	let template;

	// fetch the compiled template from cache 
	if(!templates.has(strings)) {
		template = new CompiledTemplate(strings, values);
		templates.set(strings, template);
	}
	else {
		template = templates.get(strings);
	}
	return new TemplateResult(template, values);
}