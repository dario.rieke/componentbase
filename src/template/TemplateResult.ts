import { CompiledTemplate, MATCH_PLACEHOLDER } from "./CompiledTemplate.js";
import { AttributeNamePart, AttributeValuePart, EventListenerPart, isTemplateResultIterable, NodePart, Part, TemplatePart, updateParts } from "./TemplatePart.js";
import { resolveValue } from "./util.js";

/**
 * node where we render into
 */
export type RenderRoot = HTMLElement | ShadowRoot;

/**
 * cache for template parts, so they will be generated only once
 */
const cachedParts = new Map< RenderRoot, Map<string, TemplatePart> >();

/**
 * template based on CompiledTemplate
 * which can be rendered into a node
 */
export class TemplateResult {

	/**
	 * array of the parts which can be updated
	 * this will only be populated after first render
	 * because there, all references will be set
	 */
	private _parts: Map<number, TemplatePart> = new Map();
	public readonly values: any[];
	protected template: HTMLTemplateElement;
	public compiledTemplate: CompiledTemplate;
	
	constructor(compiledTemplate: CompiledTemplate, values: any[]) {
		this.compiledTemplate = compiledTemplate;
		this.values = values;
	}

	set parts(value) {
		this._parts = value;
	}
	get parts() {
		return this._parts;
	}
}




/**
 * helper to determine if initial (re)rendering of  
 * of the template is needed
 * this includes setting up node references 
 */
function needsInitialRender(template: TemplateResult, elementToRenderTo?: RenderRoot): boolean {
	return !cachedParts.has(elementToRenderTo);
}

/**
 * renders a template to an element initially,
 * setting up references and lazily loading the nested parts 
 * 
 * omitting elementToRenderTo  prevents the template result from being cached
 * @param template template to render
 * @param eventContext context for declarative event listeners `this`
 * @returns the final document fragment to append to the DOM
 */
export function initialRender(template: TemplateResult, elementToRenderTo?: RenderRoot, eventContext?): DocumentFragment {
	const parts = template.compiledTemplate.getParts(template.values);
	const templateEl = template.compiledTemplate.getTemplateElement();
	// replace values and register nodes
	const nodeIterator = document.createNodeIterator(
		templateEl.content,
		NodeFilter.SHOW_COMMENT | NodeFilter.SHOW_ELEMENT
	);
	while ( nodeIterator.nextNode() ) {
		switch(nodeIterator.referenceNode.nodeType) {
			case Node.COMMENT_NODE:
				const comment = nodeIterator.referenceNode as Comment;
				if(comment.nodeValue.includes('part_')) {
					const id = comment.nodeValue.trim();
					const part = parts.get(id);
					part.context = eventContext;
					const value = part.getValue();
					// update references to start and end markers
					part.start = comment.previousSibling;
					part.end = comment.nextSibling;
					// insert node to dom
					// if its a nested template, render it first
					const resolved = resolveValue(value);
					
					// render nested template
					if(resolved instanceof TemplateResult) {
						const content = initialRender(resolved, null, eventContext);
						part.end.parentNode.insertBefore(content, part.end);
						comment.remove();
						part.referenceNode = null;
						part.childParts = resolved.parts;
					}
					// render iterable of templates
					else if(isTemplateResultIterable(resolved)) {
						let templateResult: TemplateResult;
						const contents: DocumentFragment[] = [];
						for(templateResult of resolved) {
							contents.push(initialRender(templateResult, null, eventContext));
						}
						comment.replaceWith(...contents);
						part.referenceNode = null;
						part.childTemplates = resolved;
					}
					// render text node
					else {
						const newNode = document.createTextNode(resolved);
						part.referenceNode = newNode;
						comment.replaceWith(newNode);	
					}
				}
				break;
			case Node.ELEMENT_NODE:
				// replace attributes and register nodes
				const element = nodeIterator.referenceNode as HTMLElement;
				if(element.hasAttribute('has_attribute_parts')) {
					element.removeAttribute('has_attribute_parts');
					// loop through attributes
					const attributes = Array.from(element.attributes);
					const attrLength = attributes.length;
					for (let i = 0; i < attrLength; i++) {
						const attribute = attributes[i];
						// handle attribute names
						if(attribute.name.includes('part_')) {
							const part = parts.get(attribute.name) as AttributeNamePart;
							part.referenceNode = element;
							part.context = eventContext;
							element.removeAttribute(attribute.name);
							const attrName: string = resolveValue(part.getValue());
							if(attrName.length) element.setAttribute(attrName, part.attributeValue);
						}
						// handle attribute values
						if(attribute.value.includes('part_')) {
							// do we have an event listener registration ?
							if(attribute.name.startsWith('@')) {
								const part: EventListenerPart = parts.get(attribute.value);
								part.context = eventContext;
								let value = part.getValue();
								part.referenceNode = element;
								// bind event context so event listeners
								// have access to the context/component as `this`
								value = value.bind(eventContext);
								part.setValue(value);
								element.removeAttribute(attribute.name);
								element.addEventListener(part.eventName, value);
								// add event listener meta data to node
								// so we can remove it easily later 
								if(!element.hasOwnProperty('_eventListeners')) (element as any)._eventListeners  = new Map();
								(element as any)._eventListeners.set(part.eventName, value); 
							}
							else {
								// handle normal attribute value binding
								//replace all parts with values
								let valueMatches = attribute.value.split(MATCH_PLACEHOLDER);
								let attrValue = '';
								valueMatches.forEach(text => {
									if(text.includes('part_')) {
										const part = parts.get(text) as AttributeValuePart;
										part.context = eventContext;
										const value = resolveValue(part.getValue());
										part.referenceNode = element;
										part.start = attrValue.length;
										attrValue += value;
										part.end = part.start + value.length;
									}
									else attrValue += text;
								});
								element.setAttribute(attribute.name, attrValue);
							}
						}
					}
				}
				break;
			default:
				break;
		}
	}
	template.parts = parts;
	//add parts to cache  
	if(elementToRenderTo) {
		cachedParts.set(elementToRenderTo, parts);
	}
	return templateEl.content;
}

/**
 * renders a template into a given render root
 * 
 * @param template 			template to render
 * @param elementToRenderTo render root to render the tempalte to
 * @param eventContext		event context which will be the `this`
 * 							context in declarative event listeners (@event)
 */
export function render(template: TemplateResult, elementToRenderTo?: RenderRoot, eventContext = null) {
	if(needsInitialRender(template, elementToRenderTo)) {
		const renderedContent = initialRender(template, elementToRenderTo, eventContext);
		elementToRenderTo.appendChild(renderedContent);
	}
	else {
		// fetching from cache IS POSSIBLY NOT WORKING FOR MULTIPLE PARTS 
		// IN THE SAME RenderRoot, test it 
		const parts = cachedParts.get(elementToRenderTo);
		const values = template.values;
		updateParts(parts, values);
	}
}