/**
 * constructor type for mixin
 */
export type Constructor<T = {}> = new (...args: any[]) => T;