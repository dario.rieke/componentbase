import { ComponentBase } from "./ComponentBase";
import { resolveValue } from "./template/util.js";

/**
 * types which the property can have
 */
type PropertyType = String | Array<any> | Object | Boolean | Number;

/**
 * converter which turns a property name into an aatribute name 
 */
const defaultPropertyToAttributeNameConverter = (propName: PropertyKey) => propName.toString().toLowerCase(); 

/**
 * optionsdescribing the property
 * which can be passed to property decorator
 */
export interface PropertyOptions {
	/**
	 * optional setter for property
	 * if you use it, make sure to call update() yourself
	 * this refers to the component
	 */
	set?: (prop, value) => void;

	/**
	 * optional getter for property
	 * if you use it, make sure to call update() yourself
	 * this refers to the component
	 */
	get?: (prop) => unknown;

	/**
	 * converter from property 
	 * to attribute
	 * this refers to the component
	 */
	toAttribute?: (value, type: PropertyType) => string;

	/**
	 * converter from attribute
	 * to property
	 * this refers to the component
	 */
	toProperty?: (value: string, type: PropertyType) => unknown;

	/**
	 * function which returns wether the property should be changed or not
	 * depending on a changed attribute
	 * 
	 * this function runs in the attributeChangedCallback
	 * it will not run on direct property setting, as this always triggers an update
	 * 
	 * this refers to the component
	 */
	hasChanged?: (oldValue: string, value: string, options: PropertyOptions) => boolean;

	/**
	 * the type of the property
	 * should be set to correctly convert
	 * from Attribute to property
	 */
	type?: PropertyType;

	/**
	 * if set to true,
	 * changing a property will emit 
	 * a custom event to react to changes
	 * from outside of the component
	 */
	emitEvent?: boolean;

	/**
	 * controls if the property should be reflected as an attribute
	 */
	attribute?: boolean;

	/**
	 * name of the corresponding attribute 
	 * function which returns the attribute name
	 */
	attributeName?: (propertyName: PropertyKey) => string;
}

/**
 * default minimum options 
 * in case required properties are not set
 */
export const DefaultPropertyOptions: PropertyOptions = {
	get: function(prop): any {
		return (this as ComponentBase)._properties.get(prop);
	},
	set: function(prop, value): void {
		(this as ComponentBase)._properties.set(prop, value);
		this.update(prop, value);
	},
	toAttribute: function(value, type) {
		switch (type) {
			case String:
			case Boolean:
			case Number:
				return value; 
			case Object:
			case Array:
				return resolveValue(JSON.stringify(value))
			default:
				return value;
		}
	},
	toProperty: function(value, type) {
		switch (type) {
			case String:
				return value;
			case Boolean:
				if(value !== null || value === '') return true;
				else return false;
			case Object:
			case Array:
				return JSON.parse(value)
			case Number: 
				return Number(value);
			default:
				return value;
		}
	},
	type: String,
	emitEvent: false,
	attribute: true,
	attributeName: defaultPropertyToAttributeNameConverter,
	hasChanged: function(oldV, newV, options) {
		if((this as ComponentBase)._isUpdating) return false;
		switch(options.type) {
			case Boolean: 
				/* if oldV is null, the attribute was added just now */ 
			 	/* if newV is null, the attribute was removed */ 
				return newV === null || oldV === null;
			case Array:
				return (oldV === null && newV !== null) || oldV.length !== newV.length || oldV !== newV;
			default: 
				return oldV !== newV;
		}

	} 
}