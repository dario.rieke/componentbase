import { ComponentBase } from "../ComponentBase.js";
import { PropertyOptions, DefaultPropertyOptions } from "../PropertyOptions.js"; 

/**
 * property decorator
 * 
 * turns a property into a reactive property
 * 
 * @param options options for the reactive property
 */
export const property = (options: PropertyOptions = DefaultPropertyOptions): CallableFunction => {
	//handle property options
	if(options !== DefaultPropertyOptions) 
		options = { ...DefaultPropertyOptions, ...options }

	return (target: typeof ComponentBase, prop: PropertyKey, descriptor: PropertyDescriptor): void => {
		/**
		 * keep track of all properties in a static property
		 * idea from: https://stackoverflow.com/questions/43912168/typescript-decorators-with-inheritance
		 */
		const ctor = (target.constructor as any);

		if (!Object.getOwnPropertyDescriptor(ctor, "properties")) {
			if (ctor.properties)
				ctor.properties = new Map(ctor.properties);
			else
				ctor.properties = new Map();
		}
		ctor.properties.set(prop, options);	
	};
}