/**
 * customElement decorator
 * 
 * defines the decorated class as a custom html tag
 * @param tagname the name of the tag to define
 * @returns 
 */
 export const customElement = (tagname: string) => (cls) => {
    window.customElements.define(tagname, cls);
}