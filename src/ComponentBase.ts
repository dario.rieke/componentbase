import { UpdatingElement } from "./UpdatingElement.js";
import { render, RenderRoot, TemplateResult } from "./template/TemplateResult.js";

/**
 * base class to extend
 * keeps attributes and properties in sync and renders the element
 */
export class ComponentBase extends UpdatingElement {
	
	/**
	 * used to defer rendering after element is connected
	 * otherwise constructor would cause a race condition
	 */
	protected _shouldRender = false;

	/**
	 * promise which resolves on render finish
	 * gets created on updating the component
	 */
	public reRender: Promise<any>;
	
	/**
	 * promise which resolves on first render
	 * promise gets created in connected callback
	 */
	public firstRender: Promise<any>;
	
	/**
	 * element to render the template into
	 * defaults to shadow dom
	 */
	public renderRoot: RenderRoot;

	/**
	 * all stylesheets which apply to the component
	 * only evaluated once for all instances
	 */
	protected static _stylesheets: CSSStyleSheet[]; 

	constructor() {
		super();
		// create render root
		this.renderRoot = this.createRenderRoot();
	}

	/**
	 * create and return the render root where the 
	 * elements content will render to
	 * 
	 * defaults to shadow root, override in child class to set
	 * up element itself as render node
	 * 
	 * @returns RenderRoot
	 */
	public createRenderRoot(): RenderRoot {
		return this.attachShadow({
			mode: 'open'
		});
	}

	/**
	 * render template on connecting
	 */
	connectedCallback() {
		this._shouldRender = true;
		this.firstRender = this._render();
		this.applyStylesheets();
		this.firstRender.then(() => {
			if('firstUpdated' in this) (this as any).firstUpdated();
		});

	}

	/**
	 * updates and rerenders the component
	 */
	update(prop: PropertyKey, value: unknown) {
		super.update(prop, value);
		// only render if connected
		if(this._shouldRender) { 
			this.reRender = this._render();
			this.reRender.then(() => {
				if('updated' in this) (this as any).updated(prop, value);
			})
		}
	}
	
	/**
	 * render the component
	 * @returns {Promise}
	 */
	protected async _render() {
		if('render' in (this as ComponentBase)) {
			const template = ((this as any).render() as TemplateResult);
			render(template, this.renderRoot, this);
		}
		return Promise.resolve();
	}

	/**
	 * apply stylesheets to component
	 * replaces existing stylesheets
	 * @param sheet 
	 */
	protected applyStylesheets() {
		if(!('styles' in this.constructor)) return;
		// render to shadow dom
		if(this.renderRoot.nodeName === "#document-fragment") {
			(this.renderRoot as any).adoptedStyleSheets = (this.constructor as any).styles;
		}
		// if render root is not shadow dom, do nothing
		else {
			console.warn(`
				${this.constructor} has static styles defined, 
				but the renderRoot is not a ShadowDom. 
				No styles will be inserted. You have to load them yourself.
			`);
			return;
		}
		// (this.constructor as typeof ComponentBase)._stylesheets;
	} 
}