var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { UpdatingElement } from "./UpdatingElement.js";
import { render } from "./template/TemplateResult.js";
/**
 * base class to extend
 * keeps attributes and properties in sync and renders the element
 */
export class ComponentBase extends UpdatingElement {
    constructor() {
        super();
        /**
         * used to defer rendering after element is connected
         * otherwise constructor would cause a race condition
         */
        this._shouldRender = false;
        // create render root
        this.renderRoot = this.createRenderRoot();
    }
    /**
     * create and return the render root where the
     * elements content will render to
     *
     * defaults to shadow root, override in child class to set
     * up element itself as render node
     *
     * @returns RenderRoot
     */
    createRenderRoot() {
        return this.attachShadow({
            mode: 'open'
        });
    }
    /**
     * render template on connecting
     */
    connectedCallback() {
        this._shouldRender = true;
        this.firstRender = this._render();
        this.applyStylesheets();
        this.firstRender.then(() => {
            if ('firstUpdated' in this)
                this.firstUpdated();
        });
    }
    /**
     * updates and rerenders the component
     */
    update(prop, value) {
        super.update(prop, value);
        // only render if connected
        if (this._shouldRender) {
            this.reRender = this._render();
            this.reRender.then(() => {
                if ('updated' in this)
                    this.updated(prop, value);
            });
        }
    }
    /**
     * render the component
     * @returns {Promise}
     */
    _render() {
        return __awaiter(this, void 0, void 0, function* () {
            if ('render' in this) {
                const template = this.render();
                render(template, this.renderRoot, this);
            }
            return Promise.resolve();
        });
    }
    /**
     * apply stylesheets to component
     * replaces existing stylesheets
     * @param sheet
     */
    applyStylesheets() {
        if (!('styles' in this.constructor))
            return;
        // render to shadow dom
        if (this.renderRoot.nodeName === "#document-fragment") {
            this.renderRoot.adoptedStyleSheets = this.constructor.styles;
        }
        // if render root is not shadow dom, do nothing
        else {
            console.warn(`
				${this.constructor} has static styles defined, 
				but the renderRoot is not a ShadowDom. 
				No styles will be inserted. You have to load them yourself.
			`);
            return;
        }
        // (this.constructor as typeof ComponentBase)._stylesheets;
    }
}
