/**
 * types which the property can have
 */
declare type PropertyType = String | Array<any> | Object | Boolean | Number;
/**
 * optionsdescribing the property
 * which can be passed to property decorator
 */
export interface PropertyOptions {
    /**
     * optional setter for property
     * if you use it, make sure to call update() yourself
     * this refers to the component
     */
    set?: (prop: any, value: any) => void;
    /**
     * optional getter for property
     * if you use it, make sure to call update() yourself
     * this refers to the component
     */
    get?: (prop: any) => unknown;
    /**
     * converter from property
     * to attribute
     * this refers to the component
     */
    toAttribute?: (value: any, type: PropertyType) => string;
    /**
     * converter from attribute
     * to property
     * this refers to the component
     */
    toProperty?: (value: string, type: PropertyType) => unknown;
    /**
     * function which returns wether the property should be changed or not
     * depending on a changed attribute
     *
     * this function runs in the attributeChangedCallback
     * it will not run on direct property setting, as this always triggers an update
     *
     * this refers to the component
     */
    hasChanged?: (oldValue: string, value: string, options: PropertyOptions) => boolean;
    /**
     * the type of the property
     * should be set to correctly convert
     * from Attribute to property
     */
    type?: PropertyType;
    /**
     * if set to true,
     * changing a property will emit
     * a custom event to react to changes
     * from outside of the component
     */
    emitEvent?: boolean;
    /**
     * controls if the property should be reflected as an attribute
     */
    attribute?: boolean;
    /**
     * name of the corresponding attribute
     * function which returns the attribute name
     */
    attributeName?: (propertyName: PropertyKey) => string;
}
/**
 * default minimum options
 * in case required properties are not set
 */
export declare const DefaultPropertyOptions: PropertyOptions;
export {};
