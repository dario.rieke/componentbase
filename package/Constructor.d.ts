/**
 * constructor type for mixin
 */
export declare type Constructor<T = {}> = new (...args: any[]) => T;
