/**
 * returns a change event to dispatch when
 * a reactive property changed
 */
const getChangeEvent = (prop, value) => new CustomEvent(prop.toString(), { detail: value });
/**
 * base class to extend
 *
 * provides reactive properties and keeps track of them
 */
export class UpdatingElement extends HTMLElement {
    constructor() {
        super();
        /**
         * internal lazy property storage
         * which property getters and setters work with
         */
        this._properties = new Map();
        /**
         * indicate wether the element is currently updating a property
         * used to prevent attributeChangedCallback from changing a property
         * twice if setAttribute is called inside update
         */
        this._isUpdating = null;
    }
    /**
     * list attributes which should be observed
     */
    static get observedAttributes() {
        // make sure all properties are set
        this.finalize();
        const attributes = [];
        this.properties.forEach((options, key) => {
            attributes.push(key.toString().toLowerCase());
        });
        return attributes;
    }
    /**
     * make sure all properties have been declared,
     * otherwise observedAttributes would not work
     */
    static finalize() {
        var _a;
        if (this.hasOwnProperty('_finalized'))
            return;
        // const parentCtor = Object.getPrototypeOf(this.constructor);
        // parentCtor.finalize();
        this._finalized = true;
        // get own and parent properties
        // overwrite the properties
        const finalPropMap = new Map([...(_a = Object.getPrototypeOf(this)) === null || _a === void 0 ? void 0 : _a.properties, ...this.properties]);
        this.properties = finalPropMap;
        this._attributePropertyMap = new Map();
        for (let [prop, options] of finalPropMap) {
            this._attributePropertyMap.set(options.attributeName(prop), prop);
            this.createProperty(prop, options);
        }
    }
    /**
     * create a reactive property
     */
    static createProperty(prop, options) {
        this.finalize();
        // create getters and setters for the property
        const getter = function () {
            return options.get.call(this, prop);
        };
        const setter = function (val) {
            if (options.emitEvent) {
                const event = getChangeEvent(prop, val);
                this.dispatchEvent(event);
            }
            options.set.call(this, prop, val);
        };
        // use this.prototype instead of Object.getPrototypeOf(this), 
        // because this method is static
        Object.defineProperty(this.prototype, prop, {
            get: getter,
            set: setter,
        });
    }
    /**
     * fired when observed attribute changes
     */
    attributeChangedCallback(attr, oldValue, newValue) {
        const prop = this.constructor._attributePropertyMap.get(attr);
        const options = this.constructor.properties.get(prop);
        if (options.hasChanged.call(this, oldValue, newValue, options))
            this[prop] = options.toProperty(newValue, options.type);
    }
    /**
     * returns the attribute name for a given property
     * @param prop
     */
    static getAttributeName(prop) {
        return String(prop).toLowerCase();
    }
    /**
     * update the component
     *
     * should be called whenever a reactive property changes
     * keeps reactive properties and attributes in sync
     */
    update(prop, value) {
        // prevent from getting called 2 times because of bool attribute
        // indicates that the prop is updating and should not be updated again while updating
        // this does only work, because attributeChangedCallback works synchronously
        this._isUpdating = prop;
        const options = this.constructor.properties.get(prop);
        if (options.attribute) {
            switch (options.type) {
                case Boolean:
                    this.toggleAttribute(options.attributeName(prop), Boolean(options.toAttribute(value, options.type)));
                    break;
                default:
                    this.setAttribute(options.attributeName(prop), options.toAttribute(value, options.type));
                    break;
            }
        }
        this._isUpdating = null;
    }
}
/**
 * holds all reactive properties
 * for the subclass
 * will be populated completely, after finalized() has been called
 *
 * maybe this should be changed, in case the original properties
 * must be accessed without inherited ones
 */
UpdatingElement.properties = new Map();
/**
 * map of attribute names and property names
 */
UpdatingElement._attributePropertyMap = new Map();
