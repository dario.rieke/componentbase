import { UpdatingElement } from "./UpdatingElement.js";
import { RenderRoot } from "./template/TemplateResult.js";
/**
 * base class to extend
 * keeps attributes and properties in sync and renders the element
 */
export declare class ComponentBase extends UpdatingElement {
    /**
     * used to defer rendering after element is connected
     * otherwise constructor would cause a race condition
     */
    protected _shouldRender: boolean;
    /**
     * promise which resolves on render finish
     * gets created on updating the component
     */
    reRender: Promise<any>;
    /**
     * promise which resolves on first render
     * promise gets created in connected callback
     */
    firstRender: Promise<any>;
    /**
     * element to render the template into
     * defaults to shadow dom
     */
    renderRoot: RenderRoot;
    /**
     * all stylesheets which apply to the component
     * only evaluated once for all instances
     */
    protected static _stylesheets: CSSStyleSheet[];
    constructor();
    /**
     * create and return the render root where the
     * elements content will render to
     *
     * defaults to shadow root, override in child class to set
     * up element itself as render node
     *
     * @returns RenderRoot
     */
    createRenderRoot(): RenderRoot;
    /**
     * render template on connecting
     */
    connectedCallback(): void;
    /**
     * updates and rerenders the component
     */
    update(prop: PropertyKey, value: unknown): void;
    /**
     * render the component
     * @returns {Promise}
     */
    protected _render(): Promise<void>;
    /**
     * apply stylesheets to component
     * replaces existing stylesheets
     * @param sheet
     */
    protected applyStylesheets(): void;
}
