import 'construct-style-sheets-polyfill';
/**
 * function to tag a css template literal and turn it
 * into a css stylesheet object
 * @param strings
 * @param values
 * @returns
 */
export declare function css(strings: TemplateStringsArray, ...values: any[]): CSSStyleSheet;
