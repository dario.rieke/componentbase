import { TemplateResult } from "./TemplateResult.js";
/**
 * return a template which can be rendered to an element
 * @param strings
 * @param values
 * @returns
 */
export declare function html(strings: TemplateStringsArray, ...values: any[]): TemplateResult;
