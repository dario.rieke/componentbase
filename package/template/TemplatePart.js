import { initialRender, TemplateResult } from "./TemplateResult.js";
import { removeBetweenNodes, resolveValue } from "./util.js";
/**
 * updatable node base class
 */
export class Part {
    constructor(id, value) {
        this.value = value;
        this.id = id;
    }
    update(value) {
        this.value = value;
    }
    getValue() {
        return this.value;
    }
    setValue(value) {
        this.value = value;
    }
    clone() {
        return new this.constructor(this.id, this.value);
    }
    hasChanged(value) {
        return value !== this.value;
    }
}
/**
 * template or text part inside an element
 */
export class NodePart extends Part {
    update(value) {
        const oldVal = this.value;
        super.update(value);
        // handle template result
        if (value instanceof TemplateResult) {
            // initial render if old value was a different template
            // or an iterable of templates
            if ((oldVal instanceof TemplateResult
                && value.compiledTemplate !== oldVal.compiledTemplate)
                || isTemplateResultIterable(value)) {
                // underlying template changed (conditional template)
                // render initially
                const content = initialRender(value, null, this.context);
                removeBetweenNodes(this.start, this.end);
                this.end.parentNode.insertBefore(content, this.end);
                // this.referenceNode = null;
                this.childParts = value.parts;
            }
            // first render of template (initially or in condition when
            // before value was a text node), after first render referenceNode will be unset/replaced
            if (this.referenceNode !== null) {
                // here, the parts value will be outdated
                // if we had a text previously
                const content = initialRender(value, null, this.context);
                this.referenceNode.replaceWith(content);
                this.referenceNode = null;
                // add parts on initial render, because otherwise they
                // will be lost on next render (only populated lazily after "initialRender")
                this.childParts = value.parts;
            }
            // update
            // TODO: dont update after initial render, 
            // just pass the newest values to initial render
            // so we dont do unneccesary work
            updateParts(this.childParts, value.values);
        }
        // render iterable of template results
        else if (isTemplateResultIterable(value)) {
            // we always completely rerender the whole iterable for now
            // TODO: somehow cache the template results
            removeBetweenNodes(this.start, this.end);
            let templateResult;
            // render templates
            for (templateResult of value) {
                const fragment = initialRender(templateResult, null, this.context);
                this.end.parentNode.insertBefore(fragment, this.end);
            }
        }
        // treat as text
        else {
            if (oldVal instanceof TemplateResult || isTemplateResultIterable(value)) {
                // remove old rendered template and replace with text node
                removeBetweenNodes(this.start, this.end);
                const textNode = document.createTextNode(value);
                this.end.parentNode.insertBefore(textNode, this.end);
                this.referenceNode = textNode;
            }
            else {
                // maybe wont work as reference node COULD be null, but no problems yet
                this.referenceNode.textContent = value;
            }
        }
    }
    /**
     * overwrite to include all props
     */
    clone() {
        const _new = new this.constructor(this.id, this.value);
        _new.start = this.start;
        _new.referenceNode = this.referenceNode;
        _new.end = this.end;
        _new.childParts = this.childParts;
        return _new;
    }
}
/**
 * attribute part in template
 */
export class AttributeNamePart extends Part {
    update(value) {
        const oldAttrName = this.value;
        super.update(value);
        // value is the attribute name, right now we assume that the inserted value is the 
        // whole name of the attribute, not only a part of it which will not work right now
        // not sure if a dynamic combination of attribute and value works either...
        if (this.referenceNode.hasAttribute(oldAttrName)) {
            this.attributeValue = this.referenceNode.getAttribute(oldAttrName);
        }
        this.referenceNode.removeAttribute(oldAttrName);
        if (value)
            this.referenceNode.setAttribute(value, this.attributeValue);
    }
    /**
     * overwrite to include all props
     */
    clone() {
        const _new = new this.constructor(this.id, this.value);
        _new.attributeValue = this.attributeValue;
        return _new;
    }
}
/**
 * attribute value part in template
 */
export class AttributeValuePart extends Part {
    update(value) {
        const oldValue = String(this.value);
        super.update(value);
        const attributeValue = this.referenceNode.getAttribute(this.attributeName);
        // replace the value 
        // works for single part in attr name but not multiple
        // probably because start and end get out of sync bc of already updated parts
        const newVal = attributeValue.substring(0, this.start) + value + attributeValue.substring(this.end);
        this.referenceNode.setAttribute(this.attributeName, newVal);
        // save new position
        this.start = attributeValue.substring(0, this.start).length;
        this.end = this.start + value.length;
    }
    /**
     * overwrite to include all props
     */
    clone() {
        const _new = new this.constructor(this.id, this.value);
        _new.attributeName = this.attributeName;
        _new.start = this.start;
        _new.end = this.end;
        return _new;
    }
}
/**
 * event listener registration part
 * currently only a dummy object which does not do any work,
 * work is done inside removeBetweenNodes and initialRender
 */
export class EventListenerPart extends Part {
    update(value) {
        const oldVal = this.value;
        // super.update(value);
        // this.referenceNode.removeEventListener(this.eventName, oldVal);
        // this.referenceNode.addEventListener(this.eventName, value);
        // (this.referenceNode as any)._eventListeners[this.eventName] = value;
    }
    // /**
    //  * we never want the event listener to change
    //  * for now, because no conditional event listeners
    //  * are supported at the moment
    //  * so we trap the setValue call
    //  */
    // setValue(value) {
    // }
    /**
     * overwrite to include all props
     */
    clone() {
        const _new = new this.constructor(this.id, this.value);
        _new.eventName = this.eventName;
        return _new;
    }
}
/**
 * update already initialized parts with given values
 * @param parts
 * @param values
 */
export function updateParts(parts, values) {
    let index = 0;
    parts.forEach((part) => {
        // update only if changed
        const newValue = resolveValue(values[index]);
        if (part.hasChanged(newValue)) {
            part.update(newValue);
        }
        index++;
    });
}
/**
 * check if a value is Iterable<TemplateResult>
 * @returns boolean
 */
export function isTemplateResultIterable(value) {
    return typeof value[Symbol.iterator] === 'function' && value[0] instanceof TemplateResult;
}
