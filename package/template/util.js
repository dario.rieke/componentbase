import { TemplateResult } from "./TemplateResult.js";
/**
 * generates unique string with 18 characters
 * @returns string
 */
export function generateUniqueString() {
    const dateString = Date.now().toString(36);
    const randomness = Math.random().toString(36).substr(2);
    const result = dateString + randomness;
    //make sure result is always 18 length
    return result.length > 19 ? result.substr(1) : result;
}
;
/**
 * resolve values to prepare them for the template
 * @param value value to resolve to template
 */
export function resolveValue(value) {
    switch (typeof value) {
        case 'string':
            return escape(String(value));
            break;
        case 'number':
            return escape(String(value));
        case 'boolean':
            return escape(String(value));
        case 'object':
            // iterable
            if (typeof value[Symbol.iterator] === 'function') {
                // return the iterable if its typeof TemplateResult[]
                if (value[0] instanceof TemplateResult)
                    return value;
                return escape(value.join(" "));
            }
            else if (value instanceof Object) {
                if (value instanceof TemplateResult)
                    return value;
                return JSON.stringify(value);
            }
            return value;
            break;
        case 'function':
            // if we have a function, 
            // its an event listener registration
            return value;
        case 'undefined':
            return '';
            break;
        default:
            break;
    }
}
/**
 * transform value into valid attribute name
 * @param value transform into attribute name
 * @returns
 */
export function resolveAttributeName(value) {
    return value.toLowerCase();
}
/**
 * escape html
 * @url https://stackoverflow.com/questions/6234773/can-i-escape-html-special-chars-in-javascript
 * @param string
 * @returns string
 */
export function escape(string) {
    return string;
    // .replace(/&/g, "&amp;")
    // .replace(/</g, "&lt;")
    // .replace(/>/g, "&gt;")
    // .replace(/"/g, "&quot;")
    // .replace(/'/g, "&#039;");
}
/**
 * remove content between two sibling nodes
 * used to clear template parts
 * also removed any listeners registered via template part
 * @param start
 * @param end
 */
export function removeBetweenNodes(start, end) {
    let currentNode = start.nextSibling;
    while (currentNode) {
        if (currentNode === end)
            break;
        // remove event listeners if needed
        if (currentNode.hasOwnProperty('_eventListeners')) {
            currentNode._eventListeners.forEach((listener, eventName) => {
                currentNode.removeEventListener(eventName, listener);
            });
        }
        const nextSibling = currentNode.nextSibling;
        currentNode.parentNode.removeChild(currentNode);
        currentNode = nextSibling;
    }
}
