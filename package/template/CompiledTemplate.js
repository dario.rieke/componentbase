import { generateUniqueString, resolveAttributeName, resolveValue } from "./util.js";
import { NodePart, AttributeNamePart, AttributeValuePart, EventListenerPart } from "./TemplatePart.js";
/**
 * global placeholder match
 */
export const MATCH_PLACEHOLDER = /(part_[0-9]+_[a-z0-9]+_)/g;
/**
 * compiled template which has updatable parts
 */
export class CompiledTemplate {
    /**
     * create compiled template and template parts
     * @param strings
     * @param values
     */
    constructor(strings, values) {
        /**
         * parts
         */
        this.parts = new Map();
        /**
         * array of part ids in the order they appear
         * used for lazily creating the parts
         */
        this.partIds = [];
        // insert markers where updatable parts should appear
        var result = strings[0];
        for (var l = 0; l < values.length; ++l) {
            let id = `part_${l}_${generateUniqueString()}_`;
            // add marker id
            this.partIds.push(id);
            // result += values[l] + strings[l + 1];
            result += id + strings[l + 1];
        }
        const tmpl = document.createElement('template');
        tmpl.innerHTML = result;
        // iterate over text nodes and replace markers 
        // with comments 
        const nodeIterator = document.createNodeIterator(tmpl.content, NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT);
        // TODO: throw error if malformed html 
        var currentNode;
        // if there parts not supported in the loop,
        // the parts index would get out of sync
        // maybe throw an error in this case because template will break
        var partIndex = -1;
        while (nodeIterator.nextNode()) {
            switch (nodeIterator.referenceNode.nodeType) {
                case Node.TEXT_NODE:
                    currentNode = nodeIterator.referenceNode;
                    // extract each placeholder from text node and split text nodes
                    // so each placeholder has an own node
                    if (currentNode.nodeValue.includes('part_')) {
                        const parts = currentNode.nodeValue.split(MATCH_PLACEHOLDER);
                        parts.forEach((text) => {
                            let newNode;
                            if (text.includes('part_')) {
                                partIndex++;
                                // add markers before and after the part marker to create
                                // the start and end of the parts content, later we will know where to remove 
                                // nodes or add nodes between
                                newNode = document.createComment(text);
                                const part = new NodePart(this.partIds[partIndex], resolveValue(values[partIndex]));
                                const startMarker = document.createTextNode(' ');
                                const endMarker = document.createTextNode(' ');
                                // part.start = startMarker;
                                // part.end = endMarker;
                                this.parts.set(this.partIds[partIndex], part);
                                currentNode.parentNode.insertBefore(startMarker, currentNode);
                                currentNode.parentNode.insertBefore(newNode, currentNode);
                                currentNode.parentNode.insertBefore(endMarker, currentNode);
                            }
                            else {
                                newNode = document.createTextNode(text);
                                currentNode.parentNode.insertBefore(newNode, currentNode);
                            }
                        });
                        currentNode.remove();
                    }
                    break;
                case Node.ELEMENT_NODE:
                    currentNode = nodeIterator.referenceNode;
                    // extract attribute parts from node and tansform them so 
                    // they are later easily queryable
                    // clone array so we dont end up with already transformed attributes in our loop
                    const attributes = Array.from(currentNode.attributes);
                    const attrCount = attributes.length;
                    for (let count = 0; count < attrCount; count++) {
                        const attribute = attributes[count];
                        if (attribute.value.includes('part_')) {
                            // add an attribute to the node to easily identify it as reactive, remove it in TemplateResult
                            if (!currentNode.hasAttribute('has_attribute_parts'))
                                currentNode.setAttribute('has_attribute_parts', '');
                            // handle event listener attribute
                            if (attribute.name.startsWith('@')) {
                                partIndex++;
                                const eventName = attribute.name.substring(1);
                                if (typeof values[partIndex] !== 'function')
                                    throw TypeError('Event listener registered with @EVENT is not a function.');
                                const id = this.partIds[partIndex];
                                const listener = resolveValue(values[partIndex]);
                                const part = new EventListenerPart(id, listener);
                                part.eventName = eventName;
                                this.parts.set(this.partIds[partIndex], part);
                            }
                            // handle normal attribute binding
                            else {
                                let valueMatches = attribute.value.split(MATCH_PLACEHOLDER);
                                valueMatches.forEach(text => {
                                    if (text.includes('part_')) {
                                        partIndex++;
                                        const id = this.partIds[partIndex];
                                        const part = new AttributeValuePart(id, resolveValue(values[partIndex]));
                                        part.attributeName = resolveAttributeName(attribute.name);
                                        this.parts.set(this.partIds[partIndex], part);
                                    }
                                });
                            }
                        }
                        if (attribute.name.includes('part_')) {
                            partIndex++;
                            // add an attribute to the node to easily identify it as reactive, remove it in TemplateResult
                            if (!currentNode.hasAttribute('has_attribute_parts'))
                                currentNode.setAttribute('has_attribute_parts', '');
                            const initialVal = currentNode.hasAttribute(attribute.name) ? currentNode.getAttribute(attribute.name) : '';
                            // change attribute values to some better queryable attribute
                            const id = this.partIds[partIndex];
                            const attributeName = resolveAttributeName(values[partIndex]);
                            const part = new AttributeNamePart(this.partIds[partIndex], attributeName);
                            part.referenceNode = currentNode;
                            part.attributeValue = resolveValue(initialVal);
                            this.parts.set(this.partIds[partIndex], part);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        this.templateElement = tmpl;
    }
    /**
     * @returns the compiled template which can be updated
     * by its template parts
     */
    getTemplateElement() {
        return document.importNode(this.templateElement, true);
    }
    /**
     * returns parts which are updatable
     * always returns a fresh copy of parts
     * with updated values
     * @param values values to set to the parts
     */
    getParts(values) {
        const cloned = new Map();
        let i = 0;
        this.parts.forEach((part, id) => {
            const clonedPart = part.clone();
            clonedPart.setValue(values[i]);
            cloned.set(id, clonedPart);
            i++;
        });
        return cloned;
    }
    /**
     * returns a list of the part ids in correct order
     */
    getPartIds() {
        return this.partIds;
    }
}
