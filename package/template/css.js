import 'construct-style-sheets-polyfill';
/**
 * function to tag a css template literal and turn it
 * into a css stylesheet object
 * @param strings
 * @param values
 * @returns
 */
export function css(strings, ...values) {
    let result = strings[0];
    for (let l = 0; l < values.length; ++l) {
        result += values[l] + strings[l + 1];
    }
    const css = new CSSStyleSheet;
    css.replaceSync(result);
    return css;
}
