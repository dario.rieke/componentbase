/**
 * generates unique string with 18 characters
 * @returns string
 */
export declare function generateUniqueString(): string;
/**
 * resolve values to prepare them for the template
 * @param value value to resolve to template
 */
export declare function resolveValue(value: any): any;
/**
 * transform value into valid attribute name
 * @param value transform into attribute name
 * @returns
 */
export declare function resolveAttributeName(value: string): string;
/**
 * escape html
 * @url https://stackoverflow.com/questions/6234773/can-i-escape-html-special-chars-in-javascript
 * @param string
 * @returns string
 */
export declare function escape(string: string): string;
/**
 * remove content between two sibling nodes
 * used to clear template parts
 * also removed any listeners registered via template part
 * @param start
 * @param end
 */
export declare function removeBetweenNodes(start: Node, end: Node): void;
