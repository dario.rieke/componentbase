import { TemplatePart } from "./TemplatePart.js";
/**
 * global placeholder match
 */
export declare const MATCH_PLACEHOLDER: RegExp;
/**
 * compiled template which has updatable parts
 */
export declare class CompiledTemplate {
    /**
     * parts
     */
    protected parts: Map<string, TemplatePart>;
    /**
     * array of part ids in the order they appear
     * used for lazily creating the parts
     */
    protected partIds: string[];
    protected templateElement: HTMLTemplateElement;
    protected templateString: string;
    /**
     * create compiled template and template parts
     * @param strings
     * @param values
     */
    constructor(strings: TemplateStringsArray, values: any[]);
    /**
     * @returns the compiled template which can be updated
     * by its template parts
     */
    getTemplateElement(): HTMLTemplateElement;
    /**
     * returns parts which are updatable
     * always returns a fresh copy of parts
     * with updated values
     * @param values values to set to the parts
     */
    getParts(values: any[]): Map<any, any>;
    /**
     * returns a list of the part ids in correct order
     */
    getPartIds(): string[];
}
