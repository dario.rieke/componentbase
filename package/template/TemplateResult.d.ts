import { CompiledTemplate } from "./CompiledTemplate.js";
import { TemplatePart } from "./TemplatePart.js";
/**
 * node where we render into
 */
export declare type RenderRoot = HTMLElement | ShadowRoot;
/**
 * template based on CompiledTemplate
 * which can be rendered into a node
 */
export declare class TemplateResult {
    /**
     * array of the parts which can be updated
     * this will only be populated after first render
     * because there, all references will be set
     */
    private _parts;
    readonly values: any[];
    protected template: HTMLTemplateElement;
    compiledTemplate: CompiledTemplate;
    constructor(compiledTemplate: CompiledTemplate, values: any[]);
    set parts(value: Map<number, TemplatePart>);
    get parts(): Map<number, TemplatePart>;
}
/**
 * renders a template to an element initially,
 * setting up references and lazily loading the nested parts
 *
 * omitting elementToRenderTo  prevents the template result from being cached
 * @param template template to render
 * @param eventContext context for declarative event listeners `this`
 * @returns the final document fragment to append to the DOM
 */
export declare function initialRender(template: TemplateResult, elementToRenderTo?: RenderRoot, eventContext?: any): DocumentFragment;
/**
 * renders a template into a given render root
 *
 * @param template 			template to render
 * @param elementToRenderTo render root to render the tempalte to
 * @param eventContext		event context which will be the `this`
 * 							context in declarative event listeners (@event)
 */
export declare function render(template: TemplateResult, elementToRenderTo?: RenderRoot, eventContext?: any): void;
