import { TemplateResult } from "./TemplateResult.js";
/**
 * single part of a template which can be updated
 */
export interface TemplatePart {
    /**
     * updates the part
     * @param value the new value for the part
     */
    update(value: any): void;
    /**
     * returns the current value the part has
     */
    getValue(): string | TemplateResult;
    /**
     * set current value the part has
     */
    setValue(value: any): void;
    /**
     * reference to node which will be updated
     */
    referenceNode: Text | HTMLElement | null;
    /**
     * reference to the passed `this` context to keep it
     */
    context: any;
    /**
     * allow the part to be cloned for each individual template instance
     * the cloned part will not contain a referenceNode
     */
    clone(): TemplatePart;
    /**
     * check if the value of the element changed
     */
    hasChanged(value: string): boolean;
}
/**
 * updatable node base class
 */
export declare class Part implements TemplatePart {
    protected value: any;
    id: string;
    referenceNode: Text | HTMLElement;
    context: any;
    constructor(id: string, value: any);
    update(value: any): void;
    getValue(): any;
    setValue(value: any): void;
    clone(): any;
    hasChanged(value: any): boolean;
}
/**
 * template or text part inside an element
 */
export declare class NodePart extends Part {
    referenceNode: null | Text;
    /**
     * start and end are empty text nodes inserted
     * in compiled template so we know the
     * boundaries of our node part
     */
    start: Text;
    end: Text;
    /**
     * map of child parts the node part has
     *
     * is only set, if the node parts value
     * is a nested template result
     */
    childParts?: Map<number, TemplatePart>;
    /**
     * iterable of child templates the node has
     *
     * is only set, if the node parts value is an
     * iterable of template results
     */
    childTemplates?: Iterable<TemplateResult>;
    update(value: any): void;
    /**
     * overwrite to include all props
     */
    clone(): any;
}
/**
 * attribute part in template
 */
export declare class AttributeNamePart extends Part {
    attributeValue: string;
    referenceNode: HTMLElement;
    value: string;
    update(value: any): void;
    /**
     * overwrite to include all props
     */
    clone(): any;
}
/**
 * attribute value part in template
 */
export declare class AttributeValuePart extends Part {
    /**
     * start point inside the attribute value where part starts
     */
    start: number;
    /**
     * end point inside the attribute value where part starts
     */
    end: number;
    attributeName: string;
    referenceNode: HTMLElement;
    update(value: any): void;
    /**
     * overwrite to include all props
     */
    clone(): any;
}
/**
 * event listener registration part
 * currently only a dummy object which does not do any work,
 * work is done inside removeBetweenNodes and initialRender
 */
export declare class EventListenerPart extends Part {
    /**
     * current registered event listener
     */
    value: EventListener;
    /**
     * name of the event to listen to
     */
    eventName: string;
    update(value: any): void;
    /**
     * overwrite to include all props
     */
    clone(): any;
}
/**
 * update already initialized parts with given values
 * @param parts
 * @param values
 */
export declare function updateParts(parts: Map<any, TemplatePart>, values: any[]): void;
/**
 * check if a value is Iterable<TemplateResult>
 * @returns boolean
 */
export declare function isTemplateResultIterable(value: any): boolean;
