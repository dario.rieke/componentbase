import { DefaultPropertyOptions } from "../PropertyOptions.js";
/**
 * property decorator
 *
 * turns a property into a reactive property
 *
 * @param options options for the reactive property
 */
export const property = (options = DefaultPropertyOptions) => {
    //handle property options
    if (options !== DefaultPropertyOptions)
        options = Object.assign(Object.assign({}, DefaultPropertyOptions), options);
    return (target, prop, descriptor) => {
        /**
         * keep track of all properties in a static property
         * idea from: https://stackoverflow.com/questions/43912168/typescript-decorators-with-inheritance
         */
        const ctor = target.constructor;
        if (!Object.getOwnPropertyDescriptor(ctor, "properties")) {
            if (ctor.properties)
                ctor.properties = new Map(ctor.properties);
            else
                ctor.properties = new Map();
        }
        ctor.properties.set(prop, options);
    };
};
