/**
 * customElement decorator
 *
 * defines the decorated class as a custom html tag
 * @param tagname the name of the tag to define
 * @returns
 */
export declare const customElement: (tagname: string) => (cls: any) => void;
