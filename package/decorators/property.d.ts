import { PropertyOptions } from "../PropertyOptions.js";
/**
 * property decorator
 *
 * turns a property into a reactive property
 *
 * @param options options for the reactive property
 */
export declare const property: (options?: PropertyOptions) => CallableFunction;
