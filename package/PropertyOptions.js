import { resolveValue } from "./template/util.js";
/**
 * converter which turns a property name into an aatribute name
 */
const defaultPropertyToAttributeNameConverter = (propName) => propName.toString().toLowerCase();
/**
 * default minimum options
 * in case required properties are not set
 */
export const DefaultPropertyOptions = {
    get: function (prop) {
        return this._properties.get(prop);
    },
    set: function (prop, value) {
        this._properties.set(prop, value);
        this.update(prop, value);
    },
    toAttribute: function (value, type) {
        switch (type) {
            case String:
            case Boolean:
            case Number:
                return value;
            case Object:
            case Array:
                return resolveValue(JSON.stringify(value));
            default:
                return value;
        }
    },
    toProperty: function (value, type) {
        switch (type) {
            case String:
                return value;
            case Boolean:
                if (value !== null || value === '')
                    return true;
                else
                    return false;
            case Object:
            case Array:
                return JSON.parse(value);
            case Number:
                return Number(value);
            default:
                return value;
        }
    },
    type: String,
    emitEvent: false,
    attribute: true,
    attributeName: defaultPropertyToAttributeNameConverter,
    hasChanged: function (oldV, newV, options) {
        if (this._isUpdating)
            return false;
        switch (options.type) {
            case Boolean:
                /* if oldV is null, the attribute was added just now */
                /* if newV is null, the attribute was removed */
                return newV === null || oldV === null;
            case Array:
                return (oldV === null && newV !== null) || oldV.length !== newV.length || oldV !== newV;
            default:
                return oldV !== newV;
        }
    }
};
