import { PropertyOptions } from "./PropertyOptions";
/**
 * map of property keys and property options
 */
declare type PropertyMap = Map<PropertyKey, PropertyOptions>;
/**
 * base class to extend
 *
 * provides reactive properties and keeps track of them
 */
export declare class UpdatingElement extends HTMLElement {
    /**
     * holds all reactive properties
     * for the subclass
     * will be populated completely, after finalized() has been called
     *
     * maybe this should be changed, in case the original properties
     * must be accessed without inherited ones
     */
    static properties: PropertyMap;
    /**
     * internal lazy property storage
     * which property getters and setters work with
     */
    _properties: Record<PropertyKey, any>;
    /**
     * map of attribute names and property names
     */
    static _attributePropertyMap: Map<string, PropertyKey>;
    /**
     * flag to see if all properties are set up
     */
    static _finalized: boolean;
    /**
     * indicate wether the element is currently updating a property
     * used to prevent attributeChangedCallback from changing a property
     * twice if setAttribute is called inside update
     */
    _isUpdating: PropertyKey | null;
    constructor();
    /**
     * list attributes which should be observed
     */
    static get observedAttributes(): any[];
    /**
     * make sure all properties have been declared,
     * otherwise observedAttributes would not work
     */
    protected static finalize(): void;
    /**
     * create a reactive property
     */
    static createProperty(prop: PropertyKey, options: PropertyOptions): void;
    /**
     * fired when observed attribute changes
     */
    attributeChangedCallback(attr: string, oldValue: any, newValue: any): void;
    /**
     * returns the attribute name for a given property
     * @param prop
     */
    protected static getAttributeName(prop: PropertyKey): string;
    /**
     * update the component
     *
     * should be called whenever a reactive property changes
     * keeps reactive properties and attributes in sync
     */
    update(prop: PropertyKey, value: unknown): void;
}
export {};
