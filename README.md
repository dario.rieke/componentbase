# Component Base
A small typescript web component framework.

## Installation / Requirements 
Include as dependency in your package.json and install dependencies. 
```json
{
  "dependencies": {
    "@drieke/componentbase": "git+https://gitlab.com/dario.rieke/componentbase.git"
  }
}
```
> You need to activate experimental decorators in your `tsconfig.json` file to use the decorators.

At the moment, the package requires a bundler which will resolve node dependencies. In the future, a bundled version will be shipped too. The system should work in plain javascript too, information how to use it will be added in the future.

## Tests 
No tests have been written yet. 🦥

## Development
Run `npm run build:package` to set up typescript watcher to compile package/dist folder.  
Run `npm run build:demo` to compile the component demo index.html file (once, no watcher).

## Basic Usage 
The `ComponentBase` lets you create web components without boilerplate code. It automatically reflects changes to attributes and vice-versa. 

The `html` tag function, provides a decoupled template engine which can also be used independently from the `ComponentBase`.

To begin with, check out the basic `ClickCounter` implementation:

> The following examples use "@drieke/componentbase" for imports. To use this feature, add it as a "path" to your tsconfig.json or set it up in your preferred build tool.
```ts
import { customElement, property, ComponentBase, html } from "@drieke/componentbase";

@customElement('click-counter')
class ClickCounter extends ComponentBase {

	@property({
		type: Number
	})
	counter: number = 0;

	increase() {
		this.counter++;
	}
	
	decrease() {
		if(this.counter !== 0) this.counter--;
	}

	static styles = [
		css`
			h1 {
				color: red
			}
		`,
		css`div { background: grey }`
	];

	render() {
		return html`
			<h2>Counter Component</h2>
			<div>
				<span>Count: ${this.counter}</span>
				<button @click=${this.increase}>+</button>
				<button @click=${this.decrease}>-</button>
			</div>
		`;
	}
}
```

### @customElement(name)
The `@customElement` decorator registers your component as a custom HTML Element.

### ComponentBase
Extend the `ComponentBase` class to quickly create configurable, reactive properties which reflect to their attributes by default.

### @property(options)
Decorate class members with `@property` to make it reactive. Configure the property with `options`. 
Checkout the [PropertyOptions](https://gitlab.com/dario.rieke/componentbase/-/blob/main/src/PropertyOptions.ts#L18) interface for more information.

### render() & html
The render method returns a template literal, tagged with the `html` function.
Everytime a reactive property changes, the `render()` method will be called and the component will update. 
The template engine will only update properties that changed*, to keep the templates performant.
*(except for iterables of templates)

### styles
The static styles property should return an array of `CSSStyleSheets` which will be added to the rendered element.

> This only works if the element renders to ShadowRoot. If it renders directly to the element, `styles` will have no effect. 

If you want to include external css files, just add them as `<link>` tags to the template.

## Templating
Below is a list of supported template bindings.

### Nested Templates
```ts
	render() {
		return html`
			<h1>Element</h1>
			${this.nestedTemplate()}
			`;
	}
	nestedTemplate() {
		return html`<div>Nested</div>`;
	}
```

### Template Iterables
```ts
	@property({
		type: Array
	})
	list: any[]  = [
		'nr1', 'nr2', 'nr3'
	];

	render() {
		return html`
			<h1>Element</h1>
			${this.list.map(val =>  html`<span>${val}</span>`)}
			`;
	}
```

### Conditional Templates
```ts
	@property({
		type: Number
	})
	counter: number = 1;

	render() {
		return html`
			<h1>Element</h1>
			${ this.counter < 3 ? html`counter: "${this.counter}"` : html`counter is small` }
		`
	}
```

### Event Listener registration
```ts
	@property({
		type: Number
	})
	counter: number = 0;

	increase() {
		this.counter++;
	}

	render() {
		return html`
			<h1>Element</h1>
				<button @click=${this.increase}>+</button>
			</div>
		`;
	}
```
### Attributes
You can use the following attribute bindings: 
#### Attribute Value
```ts
`<div class="${this.list}"></div>`
`<div class="${this.list} some-other-value"></div>`
```
> Currently you can only use one bound value inside the attribute value
#### Attribute Name
```ts
`<div ${this.myAttributeName}="400"></div>`
```
Boolean attribute binding
```ts
`<div ${this.condition ? '': 'hidden'}></div>`
```

### RenderRoot
The render root determines where to render the template to. Defaults to a open ShadowRoot. To render directly to the element without shadow encapsulation, use: 
```ts
createRenderRoot(): RenderRoot {
    return this; 
}

```

## ComponentBase

### Shared Functionality between Components
You can use horizontal inheritance to include shared properties/methods into multiple components without having to extend the same parent class. This functionality is called a `Mixin`: 
```ts
import { Constructor, property, ComponentBase, customElement } from "@drieke/componentbase";

 /**
  * mixin to add toggle functionality to a component
  */
function Toggleable<T extends Constructor<ComponentBase>> (Base : T) {
  class ToggleMixin extends Base {
	constructor(...args: any[]) {
	  super(...args)
	}

	@property({
		type: Boolean
	})
	toggled: boolean
  }

  return ToggleMixin;
}

// Adding the toggle functionality to the ComponentBase class
const ToggleableComponent = Toggleable(ComponentBase);

// final element
@customElement('my-toggle')
class MyToggle extends ToggleableComponent {
	constructor() {
		super()
	}
}
```

### Update hooks
When rendering finishes, a promise will be resolved so you can rely on the element being fully rendered, for example for selecting elements in the DOM.

#### ComponentBase.updated(property, value)
The ComponentBase provides a hook which runs after the element updated. The example sets up  additional attributes on property change.
> This hook runs after each rerender, but not on the very first render.
```ts
	updated(property: string, value: any) {
		if(property === "open") {
			this.setAttribute('tabindex', this.open ? '0' : '-1');
		}
	}
```

#### ComponentBase.firstUpdated()
This callback runs once the first render finishes. 
```ts
	firstUpdated() {
		this.setAttribute('tabindex', this.open ? '0' : '-1');
	}
```

#### ComponentBase.reRender
reRender is a promise which is created when rendering will happen and resolves once the render completes.

You can listen for the promise right after altering a reactive property. 

```ts
onClick() {
	// this will trigger rerender
	this.counter++;
	// after rerender, promise resolves
	this.reRender.then(() => console.log('reRender', performance.now()))
}

```
From the outside of the component:
```ts
const element = document.querySelector('some-element');
element.counter++;
element.reRender.then(() => console.log('reRender', performance.now()))

```
